from flask import g,Flask, jsonify, request 
from flask_restful import Resource, Api 
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
import sqlite3
import os
import json
import numpy as np
import pandas as pd 
import tensorflow as tf
import tensorflow_hub as hub
import couchdb


# creating the flask app 
app = Flask(__name__) 

# creating an API object 
api = Api(app) 

#Creating the database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db' # new
SQLALCHEMY_TRACK_MODIFICATIONS = False


DATABASE = './data/main.db'
def get_db():
    db = sqlite3.connect(DATABASE)
    return db

conn = get_db()
pandas_table = (pd.read_sql_query("SELECT * FROM main_table", conn))
pandas_table_word_embeddings = list(pandas_table["word_embeddings"].values)
word_embeddings = [np.array(json.loads(i)) for i in pandas_table_word_embeddings]
pandas_table["word_embeddings"]  = word_embeddings
book_id = list(pandas_table["books__id"].values)
book_title = list(pandas_table["books_catalog_title"].values)
word_embeddings = np.matrix(word_embeddings)


def embed_useT(module):
    with tf.Graph().as_default():
        sentences = tf.placeholder(tf.string)
        embed = hub.Module(module)
        embeddings = embed(sentences)
        session = tf.train.MonitoredSession()
    return lambda x: session.run(embeddings, {sentences: x})

embed_fn = embed_useT("data/module_useT")

@app.route('/search', methods = ['GET']) 
def home(): 
    if(request.method == 'GET'): 
        return get_most_common(request)

def get_most_common(request):
    data = request.json
    book_name = [request.args.get('data')]
    tensorflow_data = embed_fn(book_name)
    tensorflow_data = np.dot(word_embeddings, np.transpose(tensorflow_data))
    # tensorflow_data = json.dumps(tensorflow_data.tolist())
    vals = [(u,v.tolist()[0][0]) for u,v in zip(book_title,tensorflow_data)]
    vals = [(u[0], u[1] , v ) for u,v in zip(vals,book_id) ]
    sorted_by_second = sorted(vals, key=lambda tup: tup[1],reverse = True)[:100]
    sorted_by_second_booklist =f7( [i[2] for i in sorted_by_second ])[:6]
    titles = list(set([i[0] for i in sorted_by_second ] ))
    # output = json.dumps(sorted_by_second)
    ls = merge(sorted_by_second_booklist)
    return jsonify({'data':(ls)}) 

def get_data_books(sorted_by_second_booklist):
    secure_remote_server = couchdb.Server('https://umichasb:asbloc@couchdb3.prtd.app/')
    db = secure_remote_server['anc']
    flat_list_books = []
    for i in sorted_by_second_booklist:
        flat_list_books.append(db[i])
    flat_list_books = [flatten_json(json.loads(json.dumps(i)))  for i in flat_list_books]
    l = {}
    for i,j in zip(sorted_by_second_booklist,flat_list_books):
        l[i] = ({'books_'+k: v for k, v in j.items()})
    return l

def get_analysis(sorted_by_second_booklist):
    df = pandas_table.loc[pandas_table['books__id'].isin(sorted_by_second_booklist )]
    df = df[df['location'] != ""]
    df = df[(df['analysis_data_3_cie64_R_40']) != 0.0 ]
    df = df.reset_index()
    dict_vals ={}
    for index,row in df.iterrows():
        book_val = (df.iloc[index]["books__id"])
        vals = df.iloc[index].to_json()
        #Adding vals
        if(book_val in dict_vals):
            dict_vals[book_val].append(vals)
        else:
            dict_vals[book_val] = [vals]
    return dict_vals

def merge(sorted_by_second_booklist):
    book_data  = list(get_data_books(sorted_by_second_booklist).values())
    analysis_data = list(get_analysis(sorted_by_second_booklist).values())
    d = []
    for k in range(len(sorted_by_second_booklist)):
        temp = {}
        l_inner =[]
        for i in analysis_data[k]:
            inner_json = json.loads(i)
            del inner_json["word_embeddings"]
            l_inner.append(inner_json)
        # l = [json.loads(i) for i in analysis_data for j in i ]
        temp["analysis"]=  l_inner
        d.append({**book_data[k], **temp})
    return d



def flatten_json(y):
    out = {}
    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '_')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '_')
                i += 1
        else:
            out[name[:-1]] = x
    flatten(y)
    return out

def f7(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

if __name__ == '__main__': 
    app.run(host= "0.0.0.0", port = 5000) 
